# -*- encoding: utf-8 -*-
import json
import difflib
import operator
import pprint
import urllib2
import urllib
from operator import attrgetter

from django.db.models import Q
from django.shortcuts import render
from django.core.validators import ValidationError
from django.core.urlresolvers import reverse
from django.utils.datastructures import MultiValueDictKeyError

from django.views.decorators.csrf import *

from django.shortcuts import render

from django.shortcuts import render_to_response, get_object_or_404
from django.http import HttpResponse, HttpResponseRedirect
from django.template import RequestContext

# Return with json
from django.core import serializers
from django.shortcuts import render
import urllib2
import urllib

from administration.models import *

GENERAL = "General"
BASELINE = u'Línea Base 2013 (PIB medio)'

def getScenarios(onlyVisible=True):
  if onlyVisible:
    return map(attrgetter('scenario'), ScenarioStyle.objects.filter(isVisible=True))
  else:
    return Scenario.objects.all()

def getBaseline():

  # We need the baseline object in order to obtain their values
  scenarioBaseline = Scenario.objects.get(name=BASELINE).id

  # These will be used to hold the values across multiple sectors
  baselineValues = []

  for sector in Sector.objects.all():

    # Obtain all the values for the baseline for a given sector
    scenarioBaselineValue = ScenarioValue.objects.filter(
      scenario_id = scenarioBaseline, sector = sector)

    arrayBaseLineValue = [float(element.value) for element in scenarioBaselineValue]

    # We add the values of the baseline across all sectors
    if not baselineValues:
      baselineValues = arrayBaseLineValue
    else:
      baselineValues = [v1 + v2 for v1,v2 in zip(baselineValues, arrayBaseLineValue)]

  return baselineValues

def getGroupedBaseline():
  # We need the baseline object in order to obtain their values
  scenarioBaseline = Scenario.objects.get(name=BASELINE).id
  # These will be used to hold the values across multiple sectors
  baselineValues = dict()

  for sector in Sector.objects.all():

    # Obtain all the values for the baseline for a given sector
    scenarioBaselineValue = ScenarioValue.objects.filter(
      scenario_id = scenarioBaseline, sector = sector).order_by('year')

    arrayBaseLineValue = [float(element.value) for element in scenarioBaselineValue]

    # We add the values of the baseline across all sectors
    baselineValues[sector.full_name] = arrayBaseLineValue

  return baselineValues

@csrf_exempt
def personalizedScenarioDataGetter(request):
  try:
    scenario_raw = request.POST[u'scenario']
    scenario = json.loads(scenario_raw)
  except Exception as e:
    return HttpResponse("Error: Invalid format", content_type='application/json')
  # The data array with all the values in the format of the highcharts chart
  data = []
  # These will be used to hold the values across multiple sectors
  baselineValues = getGroupedBaseline()
  aggregatedValues = baselineValues
  baseline = getBaseline()
  totalValues = baseline

  sectorAccumulatedValues = []
  for actionId in scenario:
    mitigationAction = MitigationAction.objects.get(id=actionId)

    try:
      # Obtain all the values for a mitigation action across all years
      actionValues = MitigationActionValue.objects.filter(action_id=actionId).order_by('year')
      agregatedSectorValues = aggregatedValues[mitigationAction.sector.full_name]

      aggregatedValue = [v1 - float(v2.value)
                         for v1, v2 in zip(agregatedSectorValues, actionValues)]
      aggregatedValues[mitigationAction.sector.full_name] = aggregatedValue

      totalValues = [v1 - float(v2.value) for v1, v2 in zip(totalValues, actionValues)]

    except MitigationActionValue.DoesNotExist:
      pass
  for sector in sorted(aggregatedValues):
    # apilo la informacion
    data.append({
        'name': sector,
        'id': sector,
        'type': 'column',
        'data': aggregatedValues[sector]})

  data.append({
      'name': "Total",
      'id': "Total",
      'type': 'spline',
      'data': totalValues})

  data.append({
      'name': "Linea base 2013",
      'id': "Linea base 2013",
      'type': 'spline',
      'data': baseline})

  years = range(2013, 2051)
  info = json.dumps({'years': years,
                     'value': data})

  return HttpResponse(info, content_type='application/json')
